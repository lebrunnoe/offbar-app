<?php 

include("header.php");
$nomjour = array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi');

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>OffBar App</title>
		<link rel="stylesheet" href="style.css" />
	</head>
	<body>
	<?php
	
	if ($_POST['commande'])
	{
		$reponse1 = $bdd->query('SELECT * FROM plats');
		$i = 0;
		$plats[0] = rien;

		while ($donnees1 = $reponse1->fetch())
		{
			$id = $nomjour[$donnees1['jour']] . $donnees1['nbjour'];
	
			if ($_POST[$id] == on)
			{
				$plats[$i] = $id;
		
				$i++;
			}
		}

		$reponse1->closeCursor();
	
		if ($plats[0] == rien)
		{
			?> <p class="alert">Vous n'avez sélectionner aucun repas.</p> <?php
		}
		else
		{
			if ($_POST['nom'] == '')
			{
				?> <p class="alert">Vous n'avez rempli votre nom.</p> <?php
			}
			else
			{
				if ($_POST['tel'] == '')
				{
					?> <p class="alert">Vous n'avez rempli votre téléphone.</p> <?php
				}
				else
				{
					if ($_POST['mail'] == '')
					{
						?> <p class="alert">Vous n'avez rempli votre email.</p> <?php
					}
					else
					{
						if ($_POST['societe'] == '')
						{
								?> <p class="alert">Vous n'avez rempli votre société.</p> <?php
						}
						else
						{
						
						}
					}
				}
			}
		}
	}

	//formulaire
	?>
		<h2>Off BAR</h2>
		<h3>Plats d'aujourd'hui</h3>
		<form method="post" action="test.php">
		<table>
		<?php
			$reponse2 = $bdd->query('SELECT * FROM plats');
		
			while ($donnees2 = $reponse2->fetch())
			{	
				if ($donnees2['nbjour'] == 1)
				{ ?>
					<tr>
						<td class="cel1"><?php echo $nomjour[$donnees2['jour']]; ?></td>
					</tr>
				<?php }	?>
				<tr>
					<td class="cel1"></td><td class="checkbox"><input type="checkbox" name="<?php echo $nomjour[$donnees2['jour']],$donnees2['nbjour']; ?>" /></td><td class="description"><?php echo $donnees2['description']; ?></td><td><input type="number" name="<?php echo "number-" . $nomjour[$donnees2['jour']],$donnees2['nbjour']; ?>" value="0" min="0" max="10" /></td>
				</tr>
			<?php } ?> 
		</table> 
		<?php $reponse2->closeCursor(); ?>
		<h4>Vos informations</h4>
			<p id="infos">
				<label for="nom" class="label">Votre nom :</label><input type="text" name="nom" id="nom" placeholder="Ex : Henri Jacques" size="30" required /><br />
				<label for="tel" class="label">Votre téléphone :</label><input type="tel" name="tel" id="tel" placeholder="Ex : 0489374927" size="30" required /><br />
				<label for="mail" class="label">Votre mail :</label><input type="email" name="mail" id="mail" placeholder="Ex : jacques.henri@vivelapomme.com" size="30" required /><br />
				<label for="societe" class="label">Votre société :</label><input type="text" name="societe" id="societe" placeholder="Ex : LesCodeursDeLextreme" size="30" required /><br />
				<label for="membre" class="label">Êtes vous membre ?</label><input type="radio" id="membre" name="membre" value="oui" />&nbsp;Oui&nbsp;<input type="radio" id="membre" name="membre" value="non" />&nbsp;Non<br />
				<label for="save" class="label">&nbsp;</label><input type="checkbox" name="save" id="save" checked required />&nbsp;Sauver mes infos<br />  
				<input type="hidden" name="commande" value="1" />
			</p>
			<br />
			<p id="bouton">
				<input type="submit" value="Commander" />
			</p>
	</body>
</html>
<?php include("header.php"); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>OffBar App</title>
		<link rel="stylesheet" href="style.css" />
	</head>
	<body>
		<h2>Off BAR</h2>
		<h3>Plats d'aujourd'hui</h3>
		<form method="post" action="index.php">
		<table>
			<tr>
				<td class="cel1">Lundi</td>
			</tr>
			<tr>
				<td class="cel1"></td><td><input type="checkbox" name="lundi1" /></td><td class="description">Poulet curry, lait de coco, basilic Thaï, riz nature</td><td><input type="number" name="lundi1" value="0" min="0" max="10" /></td>
			</tr>
			<tr>
				<td class="cel1"></td><td><input type="checkbox" name="lundi2" /></td><td class="description">Macaroni jambon-fromage</td><td><input type="number" name="lundi2" value="0" min="0" max="10" /></td>
			</tr>
			<tr>
				<td class="cel1">Mardi</td>
			</tr>
			<tr>
				<td class="cel1"></td><td><input type="checkbox" name="mardi1" /></td><td class="description">Poulet curry, lait de coco, basilic Thaï, riz nature</td><td><input type="number" name="mardi1" value="0" min="0" max="10" /></td>
			</tr>
			<tr>
				<td class="cel1"></td><td><input type="checkbox" name="mardi2" /></td><td class="description">Macaroni jambon-fromage</td><td><input type="number" name="mardi2" value="0" min="0" max="10" /></td>
			</tr>
			<tr>
				<td class="cel1">Mercredi</td>
			</tr>
			<tr>
				<td class="cel1"></td><td><input type="checkbox" name="mercredi1" /></td><td class="description">Potage maison, pain et beurre</td><td><input type="number" name="mercredi1" value="0" min="0" max="10" /></td>
			</tr>
			<tr>
				<td class="cel1">Jeudi</td>
			</tr>
			<tr>
				<td class="cel1"></td><td><input type="checkbox" name="jeudi1" /></td><td class="description">Boulets Liégeois, purée</td><td><input type="number" name="jeudi1" value="0" min="0" max="10" /></td>
			</tr>
			<tr>
				<td class="cel1"></td><td><input type="checkbox" name="jeudi2" /></td><td class="description">Nouilles sautées au dés de porc caramélisés</td><td><input type="number" name="jeudi2" value="0" min="0" max="10" /></td>
			</tr>
			<tr>
				<td class="cel1">Vendredi</td>
			</tr>
			<tr>
				<td class="cel1"></td><td><input type="checkbox" name="vendredi1" /></td><td class="description">Boulets Liégeois, purée</td><td><input type="number" name="vendredi1" value="0" min="0" max="10" /></td>
			</tr>
			<tr>
				<td class="cel1"></td><td><input type="checkbox" name="vendredi2" /></td><td class="description">Nouilles sautées au dés de porc caramélisés</td><td><input type="number" name="vendredi2" value="0" min="0" max="10" /></td>
			</tr>
		</table>
		<h4>Vos informations</h4>
			<p id="infos">
				<label for="nom" class="label">Votre nom :</label><input type="text" name="nom" id="nom" placeholder="Ex : Henri Jacques" size="30" required /><br />
				<label for="tel" class="label">Votre téléphone :</label><input type="tel" name="tel" id="tel" placeholder="Ex : 0489374927" size="30" required /><br />
				<label for="mail" class="label">Votre mail :</label><input type="email" name="mail" id="mail" placeholder="Ex : jacques.henri@vivelapomme.com" size="30" required /><br />
				<label for="societe" class="label">Votre société :</label><input type="text" name="societe" id="societe" placeholder="Ex : LesCodeursDeLextreme" size="30" required /><br />
				<label for="membre" class="label">Êtes vous membre ?</label><input type="radio" id="membre" name="membre" value="oui" />&nbsp;Oui&nbsp;<input type="radio" id="membre" name="membre" value="non" />&nbsp;Non<br />
				<label for="save" class="label">&nbsp;</label><input type="checkbox" name="save" id="save" checked required />&nbsp;Sauver mes infos<br />  
			</p>
			<br />
			<p id="bouton">
				<input type="submit" value="Commander" />
			</p>
	</body>
</html>